Vue.transition('stagger', {
    stagger: function (index) {
	//increase delay by 50ms for each item
	// but limit max delay to 300ms
	return Math.min(300, index * 50);
    }
});

new Vue({
    el: "#app",
    data: {
	header: "Arrivals",
	arrivals: []
    },
    created() {
	var $this = this;
	$.ajax({
	    'url': 'http://apis.is/flight',
	    'type': 'GET',
	    'dataType': 'json',
	    'data': {'language': 'en', 'type': 'arrivals'},
	    'success': function (response) {
		$.each(response.results, function (index, element) {
		    element.landed = false;
		    element.confirmed = false;
		    element.cancelled = false;

		    if (element.realArrival.indexOf('Landed') > -1) {
			element.landed = true;
		    } else if (element.realArrival.indexOf('Confirm') > -1) {
			element.confirmed = true;
		    } else if (element.realArrival.indexOf('Cancel') > -1) {
			element.cancelled = true;
		    }

		    $this.$data.arrivals = response.results;
		    console.log(response);
		});
	    }
	});
    }
});
