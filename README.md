# KefArrivals #

Available at arrivals.is

This is a small website that lets you easily see and filter arrivals at Keflavík airport.
Utilizes these amazing projects:

* apis.is
* Semantic-UI
* jQuery
* tablesort.js

## Coming Soon ##
* List-view